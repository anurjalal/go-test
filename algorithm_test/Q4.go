package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	count :=0
	var input []int

	for i:=1; i<=100; i++{
		input = append(input, i)
	}

	ranNum := generateRandom(0,100)
	_,step := binSrc(input, 0, len(input)-1, ranNum, count)

	if step != -1 {
		fmt.Printf("Found number %d \n", ranNum)
		fmt.Printf("with %d step", step)
	}else{
		fmt.Printf("Number not found")
	}

}

func binSrc(sortedArr []int, l int, r int, x int, count int) (index int, n int) {
	if r >= l {
		mid := l + (r - l) / 2
		if sortedArr[mid] == x {
			count+=1
			return mid, count
		}
		if sortedArr[mid] > x {
			count+=1
			return binSrc(sortedArr, l, mid-1, x, count)
		}else {
			count += 1
			return binSrc(sortedArr, mid+1, r, x, count)
		}
	}

	//return -1,-1 if not found
	return -1, -1
}

func generateRandom(min int, max int) int {
	rand.Seed(time.Now().UnixNano())
	result := rand.Intn(max - min) + min
	return result
}