package main

import "fmt"

func main() {
	input := 1234567
	var reversedDigit []int

	//get every single digit, put on a list reversed
	for input>0{
		singleNum := input%10
		reversedDigit = append(reversedDigit, singleNum)
		input = input/10
	}

	count := 1
	for j := len(reversedDigit)-1; j>0;j--{
		count *= 10
	}

	for i := len(reversedDigit)-1 ; i>=0 ; i--{
		result := reversedDigit[i]*count
		fmt.Println(result)
		count /= 10
	}
}
