package main

import (
	"fmt"
	"math"
	"sort"
)

func main() {
	input := []int{1,2,33,44,55,33,44,22,44,33,2,77,66,1,2,3,4,5,6,7,89,3,3,8,9,75,4,3,2,2,1,3}

	divider := float64(len(input))/3
	divider = math.Round(divider)

	sliceA := input[0:int(divider)]
	sliceB := input[int(divider):int(divider+divider)]
	sliceC := input[int(divider+divider):]

	sumA := sum(sliceA)
	sumB := sum(sliceB)
	sumC := sum(sliceC)

	avgA := average(sumA, len(sliceA))
	avgB := average(sumB, len(sliceB))
	avgC := average(sumC, len(sliceC))
	avgAf := fmt.Sprintf("%.2f", avgA)
	avgBf := fmt.Sprintf("%.2f", avgB)
	avgCf := fmt.Sprintf("%.2f", avgC)

	maxA := findMax(sliceA)
	maxB := findMax(sliceB)
	maxC := findMax(sliceC)

	minA := findMin(sliceA)
	minB := findMin(sliceB)
	minC := findMin(sliceC)

	sort.Sort(sort.Reverse(sort.IntSlice(sliceA)))
	sort.Sort(sort.Reverse(sort.IntSlice(sliceB)))
	sort.Sort(sort.Reverse(sort.IntSlice(sliceC)))

	fmt.Println("Reversed data : ")
	fmt.Println(sliceA, sliceB, sliceC, "\n")
	fmt.Println("Sum data : ")
	fmt.Println(sumA, sumB, sumC, "\n")
	fmt.Println("Average data : ")
	fmt.Println(avgAf, avgBf, avgCf, "\n")
	fmt.Println("Maximum data : ")
	fmt.Println(maxA, maxB, maxC, "\n")
	fmt.Println("Minimum data : ")
	fmt.Println(minA, minB, minC)

}

func sum(input []int) int {
	result := 0
	for _, v := range input{
		result +=v
	}
	return result
}

func average(sum int, n int) float64{
	newSum := float64(sum)
	newN := float64(n)
	return newSum/newN
}

func findMax(input []int) int {
	max := input[0]
	for _,v := range input{
		if v > max{
			max = v
		}
	}
	return max
}

func findMin(input []int) int {
	min := input[0]
	for _,v := range input{
		if v < min{
			min = v
		}
	}
	return min
}
