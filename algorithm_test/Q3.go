package main

import (
	"fmt"
	"sort"
	"strings"
)

func main() {
	input := "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
		"Cras interdum mi eu magna fermentum, vel luctus tellus semper. " +
		"Nunc dignissim eleifend ipsum, nec viverra mauris pellentesque non. " +
		"Fusce auctor ex id mauris egestas, quis luctus nunc pharetra. " +
		"Sed in dignissim nisi. Aliquam sed tempor urna, nec aliquam mi. " +
		"Aenean eu feugiat lacus, vel dictum eros. Nulla condimentum porttitor aliquet. " +
		"Vestibulum vehicula elit non arcu auctor maximus. " +
		"Quisque est eros, maximus nec diam faucibus, mollis odio"

	newString := ""
	alphabetMap := make(map[int]int)

	for _,v := range input{
		var newAlph string
		if v>=65 && v <=90 {
			lower := strings.ToLower(string(v))
			decLower := int(lower[0])
			alphabetMap[decLower] += 1
			if v>85 {
				newDec := 64+(5-90%v)
				newAlph = string(newDec)
			}else{
				newDec := v+5
				newAlph = string(newDec)
			}
			newString += newAlph
		}else if v>=97 && v<=122{
			alphabetMap[int(v)] += 1
			if v>117 {
				newDec := 96+(5-122%v)
				newAlph = string(newDec)
			}else{
				newDec := v+5
				newAlph = string(newDec)
			}
			newString += newAlph
		} else{
			newString += string(v)
		}

	}

	//sorting keys/alphabet
	var keys []int
	for k := range alphabetMap{
		keys = append(keys, k)
	}
	sort.Ints(keys)

	//convert every ASCII dec num back to char
	for i:=0; i< len(keys); i++{
		alphabet := string(rune(keys[i]))
		count := alphabetMap[keys[i]]
		fmt.Printf("karakter %s sebanyak %d kali \n", strings.ToUpper(alphabet), count)
	}

	fmt.Println("")
	fmt.Println(newString)

}