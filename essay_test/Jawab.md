1.
Go adalah bahasa pemrograman yang didesain di Google oleh Robert Griesemer, Rob Pike, dan Ken Thompson. 
Go adalah bahasa pemrograman open source. Go memiliki paradigma first class function, dimana function bisa di assign ke variabel dan menjadi parameter function lainnya.
Di Go, tidak memerlukan class untuk mendeklarasikan function, berbeda dengan Java dimana semua variabel dan method harus berada di dalam sebuah class.
Go mulai terkenal sejak digunakan untuk mengembangkan docker. Lalu semakin terkenal ketika software sistem komputer lainnya seperti kubernetes dibuat dengan docker.
Go bersifat static-type, jadi tipe variabel dicek pada saat proses kompilasi, bukan saat runtime (mengurangi kesalahan pada runtime).
Dibanding bahasa pemrograman lain seperti java, proses kompilasi Go lebih cepat. 
Go di-compile menjadi executable native binary, tidak seperti java yg dikompilasi menjadi bytecode dan berjalan di JVM.  
Go mendukung gRPC yaitu pemanggilan method dan pengambilan return value berupa string atau binary oleh program lain di server yang berbeda melalui protocol HTTP 2.0.


2.
### penulisan package :
`package main`
- penulisan semua fila Go yg berada di dalam suatu direktori : `package directoryname`

### import
import ditulis dengan keyword  `import` diikuti nama library didalam tandak petik dua 
   contoh : `import "math"`
   
jika lebih dari satu library yang diimport, maka gunakan kurung buka dan kurung tutup ()
    contoh : 
    `import(
    "fmt"
    "math")`

### Method
- Dalam Go, harus ada method bernama main. jika tidak, program tidak akan berjalan.
- method di Go dapat mengembalikan lebih dari satu return value.
- Penulisan method diawali keyword `func` , dilanjutkan dengan nama method, dan dilanjutkan dengan `()` .
Contoh penulisan method:
`func main() { //method body }`
method dengan argumen tanpa return type:
`func some(arg string) { //method body }`
method dengan argumen dan return type:
`func some(arg string) string { //method body }`
method dengan argumen dan multiple return type:
`func some(arg string) (bool, string) { //method body }`
   
### Ekspresi
- ekspresi pada Go tidak membutuhkan `;` (titik koma) pada bagian akhir
contoh : `fmt.Println("Contoh")`
   
### Deklarasi variable
- Jika variabel tidak diisi value, maka contoh deklarasinya : `var a string`
- Jika variabel langsung diisi value, maka contoh deklarasinya : `var a string = "contoh"`
- Jika variabel langsung diisi value, maka menuliskan keyword `var` dan tipe data adalah optional,
namun tanda `=` diganti menjadi `:=`
    contoh : `a := "contoh"`
      - assignment variabel kedua atau setelahnya menggunakan tanda `=`
    contoh : 
        `a := "contoh"
        a = "contoh2"`
        
### Perulangan 
- Pada Go tidak ada while, namun bisa menggunakan for, contoh : `for i>0 { } `
- keyword `range` biasa digunakan untuk mengiterasi semua elemen di dalam map, slice, channel, atau array
    
### Komentar
- komentar satu baris -> contoh : `// { komentar }`
- komentar lebih dari satu baris -> contoh : 
  `*/ 
  {komentar....}  
  */`